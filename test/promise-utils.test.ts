import { Deferred, delay, retry } from '../src/promise-utils';

describe('delay', () => {
  test('resolves after the given amount of time', async () => {
    const DELAY = 100;
    const start = Date.now();
    const value = await delay(DELAY, 'ok');
    const elapsed = Date.now() - start;
    expect(elapsed).toBeGreaterThanOrEqual(DELAY);
    expect(elapsed).toBeLessThan(DELAY + 10);
    expect(value).toBe('ok');
  });

  test('without value', async () => {
    const DELAY = 100;
    const start = Date.now();
    const value = await delay(DELAY);
    const elapsed = Date.now() - start;
    expect(elapsed).toBeGreaterThanOrEqual(DELAY);
    expect(elapsed).toBeLessThan(DELAY + 10);
    expect(value).toBeUndefined();
  });
});

describe('deferred', () => {
  test('constructor', () => {
    expect(new Deferred<boolean>()).toBeInstanceOf(Deferred);
    expect(new Deferred<number>()).not.toEqual(new Deferred<number>());
  });

  test('has a promise', () => {
    expect(new Deferred().promise).toBeInstanceOf(Promise); // tslint:disable-line:no-floating-promises
  });

  test('has its promise resolve when it is fulfilled', async () => {
    const deferred = new Deferred<string>();
    const promise = deferred.promise;
    deferred.resolve('ok');
    const value = await promise;
    expect(value).toBe('ok');
  });

  test('has its promise resolve when it is fulfilled', async () => {
    const deferred = new Deferred<void>();
    const promise = deferred.promise;
    deferred.resolve();
    const value = await promise;
    expect(value).toBeUndefined();
  });

  test('can only resolve once', async () => {
    const deferred = new Deferred<string>();
    const promise = deferred.promise;
    deferred.resolve('ok');
    deferred.resolve('foo');
    const value = await promise;
    expect(value).toBe('ok');
  });

  test('cannot fail after being fulfilled', async () => {
    const deferred = new Deferred<string>();
    const promise = deferred.promise;
    deferred.resolve('ok');
    deferred.reject('foo');
    const value = await promise;
    expect(value).toBe('ok');
  });

  test('has its promise fail when it is rejected', done => {
    const deferred = new Deferred<string>();
    const promise = deferred.promise;
    promise.then(done.fail).catch(error => {
      expect(error).toBe('bad');
      done();
    });
    deferred.reject('bad');
  });
});

describe('retry', () => {
  test('succeeds once the function succeeds', async () => {
    expect(async () => await retry(() => Promise.resolve())).not.toThrow();

    let count = 0;
    await retry(
      async () => {
        count++;
        if (count < 4) {
          throw new Error('nok');
        }
        return 'ok';
      },
      0,
      8
    );
    expect(count).toBe(4);
  });

  test('succeeds with the resolved value', async () => {
    expect(await retry(() => Promise.resolve('ok'))).toBe('ok');
  });

  test('fails if the function fails more than count of allowed tries', done => {
    const ERROR = new Error('foo');
    const DELAY = 100;

    let count = 0;
    const start = Date.now();

    retry<string>(
      async () => {
        count++;
        return Promise.reject(ERROR);
      },
      DELAY,
      3
    )
      .then(done.fail)
      .catch(error => {
        const elapsed = Date.now() - start;
        expect(error).toBe(ERROR);
        expect(count).toBe(3);
        expect(elapsed).toBeGreaterThanOrEqual(2 * DELAY);
        expect(elapsed).toBeLessThan(2 * DELAY + 50);
        done();
      });
  });

  test('can deal with a non async function that only throws', done => {
    const ERROR = 'foo';
    let count = 0;

    retry<string>(
      () => {
        count++;
        throw ERROR;
      },
      0,
      2
    )
      .then(done.fail)
      .catch(error => {
        expect(error).toBe(ERROR);
        expect(count).toBe(2);
        done();
      });
  });
});
