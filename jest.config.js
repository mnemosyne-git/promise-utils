module.exports = {
    roots: [
        "<rootDir>/test"
    ],
    transform: {
        "^.+\\.tsx?$": 'ts-jest'
    },
    testEnvironment: 'node',
    coverageDirectory: 'reports/coverage'
}
