const baseConfig = require('./jest.config');

module.exports = {
    ...baseConfig,
    reporters: ['default', 'jest-junit'],
    collectCoverage: true,
    collectCoverageFrom: ['src/**/*.ts']
}
