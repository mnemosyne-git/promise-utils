/**
 * Deferred are often an anti-pattern but are still needed in some cases.
 */
export class Deferred<T> {
  public promise: Promise<T>;
  public resolve!: (value: T | PromiseLike<T>) => void;
  public reject!: (reason?: unknown) => void;

  constructor() {
    this.promise = new Promise<T>((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
    Object.freeze(this);
  }
}

/**
 * Return a promise that resolves after some time.
 */
export function delay<T>(timeout: number, value?: Promise<T> | T): Promise<T> {
  return new Promise<T>(resolve => {
    // force cast or ts complains about value possibly undefined
    setTimeout(() => resolve(value as unknown as Promise<T> | T), timeout);
  });
}

/**
 * Call fn untill the promise ends up fulfilled or rejected more than a given amount of tries (default: 10).
 * Some delay is respected between each try (default: 1000ms).
 */
/* eslint-disable @typescript-eslint/no-explicit-any */
export function retry<T>(fn: (...args: any) => Promise<T>, timeout = 1000, retries = 10): Promise<T> {
  let tries = 0;

  // make sure that we deal with an function that returns a promise
  const asyncFn = async (...args: any): Promise<T> => {
    try {
      return fn(args);
    } catch (error) {
      return Promise.reject<T>(error);
    }
  };

  const tr: (...args: any) => Promise<T> = async (...args: any) => {
    tries++;
    return asyncFn.apply(asyncFn, args).catch(error => {
      if (tries >= retries) {
        // max tries reached
        throw error;
      }

      return delay(timeout, () => {
        return tr(args);
      }).then(f => f());
    });
  };

  return tr.apply(asyncFn, []);
}
/* eslint-enable @typescript-eslint/no-explicit-any */
